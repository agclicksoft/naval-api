'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhysicalActivitySchema extends Schema {
  async up () {
    const exists = await this.hasTable('physical_activities')
    
    if (!exists)
      this.create('physical_activities', (table) => {
        table.increments()
        table.integer('user_id').unsigned().references('id').inTable('users')
        table.float('distance')
        table.time('time')
        table.float('average_speed')
        table.time('pace')
        table.float('caloric')

        table.timestamps()
      })
  }

  down () {
    this.drop('physical_activities')
  }
}

module.exports = PhysicalActivitySchema
