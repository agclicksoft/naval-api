'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AbominablePerimetersSchema extends Schema {
  async up () {
    const exists = await this.hasTable('abominable_perimeters')
    
    if (!exists)
      this.create('abominable_perimeters', (table) => {
        table.increments('id')
        table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
        table.float('value')
        table.timestamps()
      })
  }

  down () {
    this.drop('abominable_perimeters')
  }
}

module.exports = AbominablePerimetersSchema
