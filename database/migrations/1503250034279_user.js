'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  async up () {
    const exists = await this.hasTable('users')
    
    if (!exists)
      this.create('users', (table) => {
        table.increments()
        table.string('email', 50).unique()
        table.string('name', 100)
        table.string('password', 255)
        table.string('brigade', 30)
        table.string('genre', 20)
        table.date('date_birth')
        table.float('height')
        table.float('weight')
        table.timestamps()
      })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
