'use strict'
const PhysicalActivity = use('App/Models/PhysicalActivity')
const Database = use('Database')

class ActivitiesController {

  async index({auth}) {
    const user = await auth.getUser()
    const results = await PhysicalActivity.query()
    .where('user_id', user.id)
    .orderBy('id', 'desc')
    .fetch()

    return results
  }

  async store({request, auth, response}) {
    const trx = await Database.beginTransaction();
    try {

      const {distance, time, average_speed, pace, caloric} = request.post()
      const user = await auth.getUser()
      const activity = await PhysicalActivity.create({
        distance, time, average_speed, pace, caloric
      }, trx)
      await activity.user().associate(user, trx)
      await trx.commit()
      return activity

    } catch (e) {
      await trx.rollback();
      return response.status(400).send({
        error: "Erro cadastrar atividade",
        message: e.message,
      });
    }
  }

}

module.exports = ActivitiesController
