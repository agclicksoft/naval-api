'use strict'

const PhysicalActivity = use('App/Models/PhysicalActivity')
const moment = use('moment')
const Database = use('Database')

class ActivitiesReportsController {

  async index({ auth, request }) {
    const user = await auth.getUser()
    const { period } = request.get()
    var results;
    const now = moment().subtract('3', 'hours')

    results = await Database.raw(`
      select EXTRACT(${period} from created_at) as date,
      sum(distance) as distance from physical_activities
      where user_id = ?
      and created_at > ?
      GROUP BY EXTRACT(${period} from created_at)
      `, [user.id, now.subtract('7', `${period}`).format('YYYY-MM-DD HH:mm:ss')])

    return results.rows
  }

}

module.exports = ActivitiesReportsController
