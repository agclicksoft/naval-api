"use strict";

const Database = use("Database");

class UsersController {
  async update({ request, auth, response }) {
    const trx = await Database.beginTransaction();
    const user = await auth.getUser();
    try {
      const {
        name,
        email,
        brigade,
        genre,
        date_birth,
        height,
        weight,
      } = request.all();
      user.merge({
        name,
        email,
        brigade,
        genre,
        date_birth,
        height,
        weight,
      });
      await user.save(trx)
      // commita a transaction
      await trx.commit();

      return response.status(201).send({
        message: "Dados alterados com sucesso!",
        data: user,
      });
    } catch (e) {
      await trx.rollback();
      console.log(e)
      return response.status(400).send({
        error: "Erro ao realizar cadastro",
        message: e.message,
      });
    }
  }
}

module.exports = UsersController;
