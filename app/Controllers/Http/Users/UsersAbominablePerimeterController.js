'use strict';

const AbominablePerimeter = use('App/Models/AbominablePerimeter')
const moment = use('moment')
const Database = use('Database')

class UsersAbominablePerimeterController {
  async index({auth}) {
    const user = await auth.getUser()
    const results = await AbominablePerimeter.query()
    .where('user_id', user.id)
    .orderBy('id', 'desc')
    .fetch()

    return results
  }

  async store({request, response, auth}) {
    const trx = await Database.beginTransaction();
    const {value} = request.post()
    const user = await auth.getUser()
    const year = moment().format('YYYY');
    const month = moment().format('MM');
    try {
      var perimeter = await AbominablePerimeter.query()
      .whereRaw('EXTRACT(year from created_at) = ?', year )
      .whereRaw('EXTRACT(month from created_at) = ?', month )
      .andWhere('user_id', user.id)
      .first()
      if (perimeter) {
        perimeter.value = value
        await perimeter.save(trx)
      } else {
        perimeter = await AbominablePerimeter.create({
          user_id: user.id, value
        }, trx)
      }
      await trx.commit();
      return response.status(200).send({
        data: perimeter
      })
    } catch (e) {
      await trx.rollback();
      return response.status(400).send({
        error: "Erro cadastrar perímetro abdominal" + year_month,
        message: e.message,
      });
    }
  }

}

module.exports = UsersAbominablePerimeterController
