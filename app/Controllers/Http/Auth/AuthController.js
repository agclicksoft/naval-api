"use strict";

const User = use("App/Models/User");
const Database = use("Database");
const Env = use("Env");
const resetedPassword = use('App/Models/ResetedPassword')

const Mail = use('Mail')

const moment = require('moment');

class AuthController {
  async register({ request, response, auth }) {
    const trx = await Database.beginTransaction();

    try {
      const {
        name,
        email,
        password,
        brigade,
        genre,
        date_birth,
        height,
        weight,
      } = request.all();

      const findUser = await User.findBy("email", email);
      if (findUser) {
        return response.status(409).send({ data: "email já cadastrado" });
      }

      const user = await User.create(
        { name, email, password, brigade, genre, date_birth, height, weight },
        trx
      );
      // commita a transaction
      await trx.commit();

      let data = await auth.withRefreshToken().attempt(email, password);


      return response.status(201).send({
        message: "Usuário cadastrado com sucesso!",
        user: user,
        data
      });
    } catch (e) {
      await trx.rollback();
      return response.status(400).send({
        error: "Erro ao realizar cadastro",
        message: e.message,
      });
    }
  }

  async login({ request, response, auth }) {
    const { email, password } = request.only(["email", "password"]);

    let data = await auth.withRefreshToken().attempt(email, password);

    const user = await User.findByOrFail("email", email);

    return response.send({ data, user: user });
  }

  async refresh({ request, response, auth }) {
    var refresh_token = request.input("refresh_token");
    console.log(request);

    if (!refresh_token) {
      refresh_token = request.header("refresh_token");
    }

    const data = await auth
      .newRefreshToken()
      .generateForRefreshToken(refresh_token);

    const user = await auth.getUser();

    return response.send({ data: data, user });
  }

  async logout({ request, response, auth }) {
    let refresh_token = request.input("refresh_token");

    if (!refresh_token) {
      refresh_token = request.header("refresh_token");
    }

    const loggedOut = await auth
      .authenticator("jwt")
      .revokeTokens([refresh_token], true);

    return response.status(204).send({});
  }

  async forgot({ request, response }) {
    const user = await User.findByOrFail("email", request.input("email"));
    const req = request;
    try {
      /**
       * Invalida qualquer outro token que tenha sido gerado anteriormente
       */
      await PasswordReset.query().where("email", user.email).delete();

      /**
       * gera um novo token para reset da senha
       */
      const reset = await PasswordReset.create({ email: user.email });

      // Envia um novo e-mail para o Usuário, com um token para que ele possa alterar a senha
      await Mail.send(
        "emails.reset",
        { user, reset, referer: req.request.headers["referer"] },
        (message) => {
          message
            .to(user.email)
            .from(Env.get("DO_NOT_ANSWER_EMAIL"))
            .subject("Solicitação de Alteração de Senha");
        }
      );

      return response.status(201).send({
        message:
          "Um e-mail com link para reset foi enviado para o endereço informado!",
      });
    } catch (error) {
      return response.status(400).send({
        message: "Ocorreu um erro inesperado ao executar a sua solicitação!",
      });
    }
  }

  async remember({ request, response }) {
    const reset = await PasswordReset.query()
      .where("token", request.input("token"))
      .where("expires_at", ">=", new Date())
      .firstOrFail();

    return response.send(reset);
  }
  async changePassword({ request, auth, response }) {
    const user = await auth.getUser();
    user.merge(request.only(["password"]));
    await user.save();
    return response.status(200).send({
      message: "Senha alterada com sucesso",
    });
  }

  async forgotPassword({request, response}) {
    try {

        const email = request.only(['email'])

        const user = await User.findBy('email', email.email)

        if (!user)
            response.status(400).send({message: 'Email não encontrado.'})

        const resetedInfo = await resetedPassword.create(email);

        const id = resetedInfo.id;
        const max = 6 - id.toString().length;
        let code = Math.random() * 1000000
        code = code.toString();
        code = code.substr(0, max);
        code = `${id}${code}`

        resetedInfo.code = code;

        await resetedInfo.save();

        await Mail.send('forgotpassword', {code}, (message) => {
          message
            .to(user.email)
            .from('Marinha do Brasil')
            .subject('Código para redefinir sua senha')
        })

        // sgMail.setApiKey(process.env.SENDGRID_API_KEY)

        // const msg = {
        //     to: email.email,
        //     from: 'sparkeepdesenvolvimento@gmail.com',
        //     subject: 'Recuperação de senha',
        //     text: 'Email teste para recuperar senha',
        //     html: `<strong>O código para redefinir a sua senha é: ${code}</strong>`,
        //   };

        // sgMail.send(msg);

        return {message: 'Email enviado com sucesso.', data: resetedInfo}
    }
    catch (error) {
        return error.message
    }
}

async validateCode({request, params, response}) {
    const {code, email} = request.only(['code', 'email'])

    const requested = await resetedPassword.query()
        .where('code', code)
        .andWhere('email', email)
        .first();

    if (!requested)
        return response.status(404).send({message: 'Código e/ou email inválido'})

    const limitTime = moment(requested.created_at).add(10, 'minutes');

    if (moment() > limitTime)
        return response.status(401).send({message: 'Link expirado.'})

    return requested
}

async resetPassword({auth, request, params, response}) {

    const {password, id} = request.only(['email', 'password', 'id']);

    const requested = await resetedPassword.find(id);

    if (!requested)
        return response.status(404).send({message: 'Código e/ou email inválido'})

    if (requested.status == 0)
        return response.status(404).send({message: 'Código já utilizado'})

    const user = await User.query()
        .where('email', requested.email)
        .first()

    user.password = password
    await user.save()

    const data = await auth.attempt(user.email, password)

    requested.status = 0;
    await requested.save();

    return {data, user};
}
}

module.exports = AuthController;
