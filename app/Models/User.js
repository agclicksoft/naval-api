'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static boot() {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }
  abominablePerimeters() {
    return this.hasMany('App/Models/AbominablePerimeter')
  }
  physicalActivities() {
    return this.hasMany('App/Models/PhysicalActivity')
  }

  //Get and set
  static get dates() {
    return super.dates.concat(["date_birth"]);
  }

  static castDates(field, value) {
    if (field == "date_birth") return value ? value.format("YYYY-MM-DD") : value;
    else return value ? value.format("YYYY-MM-DD") : value;
  }
}

module.exports = User
