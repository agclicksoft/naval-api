'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ResetedPassword extends Model {
}

module.exports = ResetedPassword
